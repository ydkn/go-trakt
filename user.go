package trakt

// UserService services wrapping the users scope for a single user.
type UserService struct {
	Client *Client
	UserID string
}

// User creates a new UserService.
func (c *Client) User(userID string) *UserService {
	return &UserService{Client: c, UserID: userID}
}
