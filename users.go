package trakt

import "context"

// UsersService services wrapping the users scope.
type UsersService struct {
	Client *Client
}

// UserSettings response for a settings request.
type UserSettings struct {
	User struct {
		Username             string `json:"username"`
		Name                 string `json:"name"`
		Private              bool   `json:"private"`
		VIP                  bool   `json:"vip"`
		VIPExecutiveProducer bool   `json:"vip_ep"`
		IDs                  struct {
			Slug string `json:"slug"`
		} `json:"ids"`
	} `json:"user"`
}

// Users creates a new UsersService.
func (c *Client) Users() *UsersService {
	return &UsersService{Client: c}
}

// Settings get user settings.
func (s *UsersService) Settings(ctx context.Context) (*UserSettings, error) {
	var result UserSettings

	err := s.Client.getRequest(ctx, "users/settings", &result)
	if err != nil {
		return &result, err
	}

	return &result, nil
}
