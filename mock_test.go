package trakt

import (
	"bytes"
	"io"
	"net/http"
)

type MockClient struct {
	DoFunc func(req *http.Request) (*http.Response, error)
}

func (m *MockClient) Do(req *http.Request) (*http.Response, error) {
	return m.DoFunc(req)
}

func MockClientWithStaticHTTPResponse(resp []byte) *Client {
	c := &Client{
		http: &MockClient{
			DoFunc: func(req *http.Request) (*http.Response, error) {
				return &http.Response{
					StatusCode: 200,
					Body:       io.NopCloser(bytes.NewReader([]byte(resp))),
				}, nil
			},
		},
	}

	return c
}

// MockClientValidateRequest is a helper function to validate properties of the request
// during unit testing.  It does not actually perform the request.
func MockClientWithValidateRequest(validator func(*http.Request) error, resp []byte) *Client {
	c := &Client{
		http: &MockClient{
			DoFunc: func(req *http.Request) (*http.Response, error) {
				if err := validator(req); err != nil {
					return nil, err
				}

				return &http.Response{
					StatusCode: 200,
					Body:       io.NopCloser(bytes.NewReader([]byte(`[]`))),
				}, nil
			},
		},
	}

	return c
}
