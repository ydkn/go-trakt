package trakt

import (
	"context"
	"fmt"
	"net/url"
)

// SearchService services wrapping the search scope.
type SearchService struct {
	Client *Client
}

// Search creates a new SearchService.
func (c *Client) Search() *SearchService {
	return &SearchService{Client: c}
}

// IDLookup find items by service id and optional type.
func (s *SearchService) IDLookup(
	ctx context.Context,
	serviceType ServiceType,
	itemID string,
	itemType ItemType,
) ([]*Item, error) {
	var result []*Item

	err := s.Client.getRequest(ctx, fmt.Sprintf("search/%s/%s?type=%s", serviceType, itemID, itemType), &result)
	if err != nil {
		return result, err
	}

	return result, nil
}

// TextQuery find items by type and query string.
func (s *SearchService) TextQuery(
	ctx context.Context,
	itemType ItemType,
	query string,
) ([]*Item, error) {
	var result []*Item

	err := s.Client.getRequest(ctx, fmt.Sprintf("search/%s?query=%s", itemType, url.QueryEscape(query)), &result)
	if err != nil {
		return result, err
	}

	return result, nil
}
