package trakt

import (
	"errors"
	"fmt"
	"net/http"
	"strings"
)

var (
	errRemovalNotConfirmed     = errors.New("removal was not confirmed")
	errAddingNotConfirmed      = errors.New("adding was not confirmed")
	errAuthFailed              = errors.New("")
	errTypeUnsupported         = errors.New("unsupported type: ")
	errScrobbleTypeUnsupported = errors.New("unsupported type for scrobble: ")
	errItemTypeInvalid         = errors.New("invalid item type: ")
)

type Error struct {
	Message           string
	Method            string
	URL               string
	ResponseCode      int
	RateLimitExceeded bool
	Parent            error
}

func newError(msg, method, url string, responseCode int, rateLimitExceeded bool, parent error) *Error {
	return &Error{
		Message:           msg,
		Method:            method,
		URL:               url,
		ResponseCode:      responseCode,
		RateLimitExceeded: rateLimitExceeded,
		Parent:            parent,
	}
}

func (e *Error) Error() string {
	parts := []string{}

	if e.Method != "" && e.URL != "" {
		if e.ResponseCode != 0 {
			parts = append(parts, fmt.Sprintf("%s %s failed with status %d (%s)", e.Method, e.URL, e.ResponseCode,
				http.StatusText(e.ResponseCode)))
		} else {
			parts = append(parts, fmt.Sprintf("%s %s failed", e.Method, e.URL))
		}
	}

	if e.Message != "" {
		parts = append(parts, e.Message)
	}

	if e.Parent != nil {
		parts = append(parts, e.Parent.Error())
	}

	return strings.Join(parts, ": ")
}
