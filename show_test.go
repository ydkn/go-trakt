package trakt

import (
	"context"
	"testing"
)

func TestLastEpisode(t *testing.T) {
	httpResponse := []byte(`{
		"season": 6,
		"number": 10,
		"title": "The Winds of Winter",
		"ids": {
		  "trakt": 1989031,
		  "tvdb": 5624261,
		  "imdb": "tt4283094",
		  "tmdb": 1187405
		}
	  }`)

	s := MockClientWithStaticHTTPResponse(httpResponse)

	resp, err := s.Show(123).LastEpisode(context.TODO())
	if err != nil {
		t.Errorf("SyncService.LastActivities returned error: %v", err)
	}

	if resp == nil {
		t.Errorf("SyncService.LastActivities returned nil")
	}

	if resp.Season != 6 {
		t.Errorf("SyncService.LastActivities returned %+v, want %+v", resp.Season, 6)
	}
}
